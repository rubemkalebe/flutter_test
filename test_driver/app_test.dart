import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Counter app', () {
    final counterTextFinder = find.byValueKey('counter');
    final buttonFinder = find.byValueKey('increment');

    FlutterDriver flutterDriver;

    setUpAll(() async {
      flutterDriver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if(flutterDriver != null) {
        flutterDriver.close();
      }
    });

    test('Counter starts at 0', () async {
      expect(await flutterDriver.getText(counterTextFinder), '0');
    });

    test('Increments the counter', () async {
      await flutterDriver.tap(buttonFinder);

      expect(await flutterDriver.getText(counterTextFinder), '1');
    });
  });
}